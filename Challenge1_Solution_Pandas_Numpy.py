import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


INPUT = r'data\HWL_comparison.csv'


def main():
    df = pd.read_csv(INPUT)
    df['diff'] = df['Surveyed WL (m)'] - df['Modelled WL (m)']

    # stats
    mean = abs(df['diff']).mean()
    rms = np.sqrt((df['diff'] ** 2).mean())

    # print to console
    print(f'Mean Difference:         {mean:.03f}')
    print(f'Root Mean Squared Error: {rms:.03f}')

    # plot
    plt.plot(df['Surveyed WL (m)'], df['Modelled WL (m)'], linestyle='', marker='o')
    plt.plot(df['Surveyed WL (m)'], df['Surveyed WL (m)'])
    plt.grid()
    plt.xlabel('Surveyed level (m)')
    plt.ylabel('Modelled level (m)')
    plt.xlim(88, 103)
    plt.ylim(88, 103)
    plt.show()


if __name__ == '__main__':
    main()