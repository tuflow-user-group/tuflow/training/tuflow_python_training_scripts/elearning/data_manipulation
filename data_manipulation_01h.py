from dateutil.parser import parse, ParserError
from datetime import datetime
import sys


OUTPUT = r'output\boundary.csv'
QC_MAX = 90
START_DATE = datetime(2012, 1, 25)
END_DATE = datetime(2012, 1, 27)


def main():
    input_file = sys.argv[1]
    gauge_data = []
    with open(input_file, 'r') as f:
        line_no = 0
        for _ in range(10):
            next(f)
            line_no += 1

        print(f'Reading file {input_file}...')
        for line in f:
            line_no += 1
            data = line.split(',')

            # convert date
            try:
                date = parse(data[0], ignoretz=True)
            except ParserError:
                print(f'ERROR: converting date data on line {line_no} - line: {line}')
                continue

            # convert level
            try:
                level = float(data[1])
            except ValueError:
                print(f'ERROR: converting level data on line {line_no} - line: {line}')
                continue

            if len(data) >= 4 and int(data[3]) <= QC_MAX and START_DATE <= date <= END_DATE:
                time = (date - START_DATE).total_seconds() / 60. / 60.
                gauge_data.append((date, time, level))

    print(f'Writing out data to {OUTPUT}...')
    with open(OUTPUT, 'w') as f:
        f.write('time (hr),level (m AHD)\n')
        for _, time, level in gauge_data:
            f.write(f'{time:.02f},{level:.03f}\n')

    print('Finished')


if __name__ == '__main__':
    main()