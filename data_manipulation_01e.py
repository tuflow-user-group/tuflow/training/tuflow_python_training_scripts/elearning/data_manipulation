from dateutil.parser import parse, ParserError
import matplotlib.pyplot as plt


INPUT = r'data\raw_levels.csv'
QC_MAX = 90


def main():
    dates = []
    levels = []
    with open(INPUT, 'r') as f:
        line_no = 0
        for _ in range(10):
            next(f)
            line_no += 1

        print(f'Reading file {INPUT}...')
        for line in f:
            line_no += 1
            data = line.split(',')

            # convert date
            try:
                date = parse(data[0], ignoretz=True)
            except ParserError:
                print(f'ERROR: converting date data on line {line_no} - line: {line}')
                continue

            # convert level
            try:
                level = float(data[1])
            except ValueError:
                print(f'ERROR: converting level data on line {line_no} - line: {line}')
                continue

            if len(data) >= 4:
                qc = int(data[3])
                if qc <= QC_MAX:
                    dates.append(date)
                    levels.append(level)

    plt.plot(dates, levels)
    plt.show()
    print('Finished')


if __name__ == '__main__':
    main()