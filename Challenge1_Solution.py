import matplotlib.pyplot as plt


INPUT = r'data\HWL_comparison.csv'


def main():
    with open(INPUT, 'r') as f:
        next(f)  # skip first line

        sum_dif = 0
        sum_dif_sq = 0
        count = 0
        surveyed_data = []
        modelled_data = []
        for line in f:
            data = line.split(',')
            surv = float(data[0])
            mod_ = float(data[1])
            diff = abs(surv - mod_)

            # collect levels
            surveyed_data.append(surv)
            modelled_data.append(mod_)

            # collect stat components
            sum_dif += diff
            sum_dif_sq += diff ** 2
            count += 1

    # stats
    mean = sum_dif / count
    mse = sum_dif_sq / count
    rms = mse ** 0.5

    # print to console
    print(f'Mean Difference:         {mean:.03f}')
    print(f'Root Mean Squared Error: {rms:.03f}')

    # plot
    plt.plot(surveyed_data, modelled_data, linestyle='', marker='o')
    plt.plot(surveyed_data, surveyed_data)
    plt.grid()
    plt.xlabel('Surveyed level (m)')
    plt.ylabel('Modelled level (m)')
    plt.xlim(88, 103)
    plt.ylim(88, 103)
    plt.show()


if __name__ == '__main__':
    main()