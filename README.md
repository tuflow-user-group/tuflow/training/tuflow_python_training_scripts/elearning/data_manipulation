These scripts are the completed examples and data from the Introduction to Python for TUFLOW Modellers eLearning course.  The course is split into two modules, data manipulation and results viewing and post processing.  This project contains the completed script examples for the data manipulation module. The Results Viewing and Post Processing Examples can be found here (https://gitlab.com/tuflow-user-group/tuflow_python_training_scripts/elearning/result_processing).

The data_manipulation_01 script is developed incrementally to the final script data_manipulation_01h.py.  The intermittent stages are as follows:

- data_manipulation_01-Read a CSV File
- data_manipulation_01a-Read a CSV File with a modern approach
- data_manipulation_01b-Parse Data
- data_manipulation_01c-Plotting Data
- data_manipulation_01d-Filtering Data
- data_manipulation_01e-Handling Exceptions
- data_manipulation_01f-Filtering Data by Date
- data_manipulation_01g-Writing Data
- data_manipulation_01h-Improved Code to be run for batch file
- data_manipulation_pandas-Use of Pandas Library


run_as_batch.bat is an example to run script data_manipulation_01h.

Challenge1_Solution, Challenge1_Solution_Pandas_Numpy and Challenge1_Solution_Pandas_Numpy_Advanced provide solutions to the Challenge 1 provided challenges to test the understanding of the data manipulation procedures introduced during the training.


