from dateutil.parser import parse
import matplotlib.pyplot as plt


INPUT = r'data\raw_levels.csv'


def main():
    dates = []
    levels = []
    with open(INPUT, 'r') as f:
        for _ in range(10):
            next(f)

        for line in f:
            data = line.split(',')
            date = parse(data[0], ignoretz=True)
            level = float(data[1])
            dates.append(date)
            levels.append(level)

    plt.plot(dates, levels)
    plt.show()
    print('Finished')


if __name__ == '__main__':
    main()