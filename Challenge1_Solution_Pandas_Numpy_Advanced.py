import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys


def main():
    input_file = sys.argv[1]
    df = pd.read_csv(input_file )
    df['diff'] = df['Surveyed WL (m)'] - df['Modelled WL (m)']

    # stats
    mean = abs(df['diff']).mean()
    rms = np.sqrt((df['diff'] ** 2).mean())

    # print to console
    print(f'Mean Difference:         {mean:.03f}')
    print(f'Root Mean Squared Error: {rms:.03f}')

    # plot
    plt.plot(df['Surveyed WL (m)'], df['Modelled WL (m)'], linestyle='', marker='o')
    plt.plot(df['Surveyed WL (m)'], df['Surveyed WL (m)'])
    plt.grid()
    plt.xlabel('Surveyed level (m)')
    plt.ylabel('Modelled level (m)')
    plt.xlim(88, 103)
    plt.ylim(88, 103)
    plt.show()


if __name__ == '__main__':
    main()